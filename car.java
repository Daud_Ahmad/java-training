package JDBCExample;
import java.util.*;
import java.sql.*;

public class Main
{   
	
	
	public static void show_command(Connection con)throws Exception
    {   
		Scanner kb= new Scanner(System.in);
        Statement st=con.createStatement();
	
        while(true)
        {   
            System.out.print("\nEnter command:");
            String cmd=kb.next();
            cmd = cmd.toLowerCase();
            switch(cmd)
            {
            
            case "add":
            {
            	ResultSet r = st.executeQuery("SELECT COUNT(*) AS rowcount FROM car");
            	r.next();
            	int count = r.getInt("rowcount") ;
            	
            	if(count==20)
            	{
            		System.out.println("Maximum Limit Exceed in DB");
            		continue;
            		
            	}
            	
            	
                String Make;
                System.out.print("Make: ");
                Make=kb.next();
                System.out.println();
                
                String Model;
                System.out.print("Model: ");
                Model=kb.next();
                System.out.println();
               
                int year;
                System.out.print("Year: ");
                year=kb.nextInt();
                System.out.println();
                
                
                float price;
                System.out.print("Sales Price ($): ");
                price=kb.nextFloat();
                System.out.println();
                            
                st.executeUpdate("insert into car(make,model,year,price) values('"+Make+"','"+Model+"',"+year+","+price+")");  

                //car[counter]=new Car(Make,Model,year,price);
                //counter++;
                
                break;
            }
            
            
            case "list":
            { 
            	ResultSet rs=st.executeQuery("Select * from car");
        		int cnt=0;
        		float tot_price=0;
        		while(rs.next())
        		{   
        			String make=rs.getString("make");
        			String model=rs.getString("model");
        			int year=rs.getInt("year");
        			float price=rs.getFloat("price");
        			
        			tot_price+=price;
        			System.out.println(make+" "+model+ " "+year+" $"+price);
        			cnt++;
        		
        		}
                if(cnt==0)
                {   
                    System.out.println("There is currently no cars in the catalog");
                    continue;
                }    
                
//                for(int i=0;i<counter;i++)
//                {
//                    System.out.println(car[i].getyear()+" "+car[i].getmake()+" "+car[i].getmodel()+"     $"+car[i].getprice());
//                    tot_price+=car[i].getprice();
//                }
               System.out.print("\n\n");
               System.out.print("Number of cars: "+cnt);
               System.out.print("\nTotal inventory:    $"+tot_price);
               
               break;
            }
            
            
            case "delete":
            {
            	System.out.println("Enter car you want to delete : ");
            	String make =kb.next();
            	
            	String sql = "DELETE FROM car WHERE make=?";
            	 
            	PreparedStatement statement = con.prepareStatement(sql);
            	statement.setString(1,make);
            	 
            	int rowsDeleted = statement.executeUpdate();
            	if (rowsDeleted > 0) {
            	    System.out.println("A car was deleted successfully!");
            	}
            	break;
            }
            
            case "update":
            {
            	System.out.print("Enter make you want to update:");
            	String make=kb.next();
            	System.out.print("Enter new Model :");
            	String model=kb.next();
            	System.out.print("Enter new year :");
            	String year=kb.next();
            	System.out.print("Enter new price :");
            	String price=kb.next();
            	String sql = "update car set model=? , year=?, price=? where make=?";
            	PreparedStatement stmt = con.prepareStatement(sql);
            	stmt.setString(1,model);
                stmt.setString(2,year);
                stmt.setString(3,price);
                stmt.setString(4,make);
                
                stmt.executeUpdate();
            	break;

            }
            case "quit":
            return ;
            
            default:
            System.out.print("Sorry, but \"" +cmd+ "\" is not a valid command. Please try again.");
           }   
        }
    }
	public static void main(String[] args) throws Exception{
		 String JdbcURL = "jdbc:mysql://localhost:3306/car";
	      String Username = "root";
	      String password = "root";
	      Connection con = null;
	      try {
	         con = DriverManager.getConnection(JdbcURL, Username, password);
	         System.out.println("Connected to MySQL database");
	      } 
	      catch (Exception e) {
	         e.printStackTrace();
	      }
		
		
		System.out.println("Welcome to Mullet Joe's Gently Used Autos!\n");
		show_command(con);
		System.out.println("\n\nGood bye!");

		
	}
}


//class Car
//{
//
//    
//    private String make;
//    private String model;
//    private int year;
//    private float price;
//     Car(String make,String model,int year,float price)
//     {
//         super();
//         this.make=make;
//         this.model=model;
//         this.year=year;
//         this.price=price;
//     }
//     public String getmake()
//     {
//         return make;
//     }
//     public String getmodel()
//     {
//         return  model;
//     }
//     public int getyear()
//     {
//         return year;
//     }
//     public float getprice()
//     {
//         return price;
//     }
//}
